import Vue from 'vue'
import VueRouter from 'vue-router'
import LatestBewery from '@/components/LatestBrewery'
import Brewery from '@/components/Brewery'

Vue.use(VueRouter)
export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'LatestBrewery',
      component: LatestBewery
    },
    {
      path: '/breweries/:id',
      name: 'Brewery',
      props: true,
      component: Brewery
    }
  ],
  mode: 'history'
})
