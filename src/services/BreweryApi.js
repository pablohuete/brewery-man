import axios from 'axios'
let breweriesApi = 'https://api.openbrewerydb.org'
export default {
  async fetchBreweryCollection (amount) {
    const response = await axios.get(breweriesApi + '/breweries?per_page=' + amount)
    return response.data
  },

  async fetchSingleBrewery (id) {
    const response = await axios.get(breweriesApi + '/breweries/' + id)
    return response.data
  }
}
